<?php

class PDF extends FPDF
{


    // Cabecera de página
    function Header()
    {
        // Logo
        $this->Image('./Img/logo.png', 10, 8, 55);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this->Cell(130);
        // Título
        $this->Cell(6, 10, iconv('UTF-8', 'windows-1252','PEDIDO Nº '), 0, 0, 'L');
        $this->Cell(25);
        $this->Cell(80, 10, $this->pedido, 0, 0, 'L');
        // Salto de línea
        $this->Ln(30);

        
        
      
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Número de página
        $this->Cell(0, 10, iconv('UTF-8', 'windows-1252','Página ') . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

    // Una tabla más completa
    function ImprovedTable(
        $header,
        $data
    ) {

            // Anchuras de las columnas
            $w = array(15,26,26,26,26,26,26,26,26,26,26);
            $h = 6;
        if ($this->EsPrimero) {
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(6, 25, iconv('UTF-8', 'windows-1252', 'CLIENTE:'), 0, 0, 'L');
            $this->SetFont('');
            $this->Cell(10);
            $this->Cell(50, 25, $data->cliente, 0, 0, 'L');
            $this->Ln(6);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(6, 25, iconv('UTF-8', 'windows-1252', 'DESTINO:'), 0, 0, 'L');
            $this->SetFont('');
            $this->Cell(10);
            $this->Cell(50, 25, $data->destino, 0, 0, 'L');
            $this->Ln(6);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(6, 25, iconv('UTF-8', 'windows-1252', 'CULTIVO:'), 0, 0, 'L');
            $this->SetFont('');
            $this->Cell(10);
            $this->Cell(50, 25, $data->cultivo, 0, 0, 'L');
            $this->Ln(6);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(6, 25, iconv('UTF-8', 'windows-1252', 'FORMATO:'), 0, 0, 'L');
            $this->SetFont('');
            $this->Cell(10);
            $this->Cell(50, 25, $data->formato, 0, 0, 'L');
            $this->Ln(6);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(6, 25, iconv('UTF-8', 'windows-1252', 'FECHA:'), 0, 0, 'L');
            $this->SetFont('');
            $this->Cell(10);
            $this->Cell(50, 25, $data->fechapedido, 0, 0, 'L');
            $this->Ln(6);
            $this->Ln(20);   
              // Cabeceras
              $this->SetFont(
                'Arial',
                'B',
                8
            );
            for ($i = 0; $i < count($header); $i++){
                $this->Cell($w[$i], 7, iconv('UTF-8', 'windows-1252', $header[$i]), 1, 0, 'L');
                    
            }
            
          
            
        }
        $pedido =$data;
        $this->Ln();
        // Datos
        $this->SetFont('');
        
      
        $ggn = explode(",",$pedido->ggn);
        $proceso = true;
        $position = 0;//determina la posicion de la celda
        $i = 0;
        
            while($proceso){
               $incremento = true;
                if($i < count($ggn)){
                    if($position != 10){
                        if($position == 0){
                            $this->Cell($w[0], $h, $i == 0 ? $pedido->palletid : '', 'LB');
                        }else{
                            $this->Cell($w[$position], $h,$ggn[$i], 'LB'); 
                            
                        }
                    }else{
                        $this->Cell($w[$position], $h,$ggn[$i], 'LBR');
                        $this->Ln();
                        $position = 0;
                        $incremento = false;

                        if( $i == ((count($ggn))-1) ){
                            $proceso = false;
                        }
                        
                    }
                }else{
                    if($position != 10){
                        
                        $this->Cell($w[$position], $h,"", 'LB');
                    }else{
                        $this->Cell($w[$position], $h," ", 'LBR');
                        $proceso = false;
                       
                    }
                }
                if($position != 0){
                    $i++;
                }
                if($incremento){
                    $position++;
                }
            }   
        
           
    
        

       
    }
}
