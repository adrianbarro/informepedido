<?php

class PDFCLIENTE extends FPDF
{


    // Cabecera de página
    function Header()
    {
        $this->SetFont('Arial', 'B', 15);
       $this->Cell(6, 25, iconv('UTF-8', 'windows-1252','Packing List '), 0, 0, 'L');
       
       $this->SetFont('Arial', '', 10);
       $this->Ln(1);
       $this->Cell(6, 35, $this->pedido, 0, 0, 'L');
       $this->Cell(20);
       $this->Cell(6, 35, 'V12', 0, 0, 'L');
       $this->Cell(100);
        
        $this->Image('./Img/LOGO ONUBAFRUIT.png', 120, 18, 100);
        // Arial bold 15
      
        // Movernos a la derecha
       
        // Título
        
        $this->Ln(14);

        
        
      
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Número de página
        $this->Cell(0, 10, iconv('UTF-8', 'windows-1252','Página ') . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

    // Una tabla más completa
    function ImprovedTable(
        $header,
        $data
    ) {

            // Anchuras de las columnas
            $w = array(26,26,20,26,26,43,35,26,26,20,26);
            $h = 6;
        if ($this->EsPrimero) {
            $this->Ln(10);
            $this->SetFont('Arial', 'B', 8);
            $this->SetFillColor(200,220,255);
            $this->Cell(25, 6, iconv('UTF-8', 'windows-1252', 'COOPERATIVE:'), 1, 0, 'L',true);
            $this->SetFont('');       
            $this->SetFillColor(255,255,255);
            $this->Cell(50, 6, "SAT condado", 1, 0, 'C',true);
            $this->Ln(6);
            $this->SetFont('Arial', 'B', 8);
            $this->SetFillColor(200,220,255);
            $this->Cell(25, 6, iconv('UTF-8', 'windows-1252', 'CUSTOMER:'), 1, 0, 'L',true);
            $this->SetFont('');
            $this->SetFillColor(255,255,255);
            $this->Cell(72, 6, $data->cliente, 1, 0, 'C',true);
            $this->Ln(10);   
             
            
            for ($i = 0; $i < count($header); $i++){
                 // Cabeceras
              $this->SetFont(
                'Arial',
                'B',
                8
                );
                $this->SetFillColor(200,220,255);
                $this->Cell($w[$i], 7, iconv('UTF-8', 'windows-1252', $header[$i]), 1, 0, 'C',true);
                    
            }
            
           
            
        }
      
        $this->Ln();
        // Datos
        $this->SetFont('');

        
       
        $this->SetFillColor(230,230,0);
       
        $this->Cell($w[0], $h,$data->fechapedido, 'LB',0,'C'); 
        $this->Cell($w[1], $h,$data->fechapedido, 'LB',0,'C'); 
        $this->Cell($w[2], $h,$data->palletid, 'LB',0,'C'); 
        $this->Cell($w[3], $h,$data->ggn, 'LB',0,'C'); 
        $this->Cell($w[4], $h,$data->cultivo, 'LB',0,'C'); 
        $this->Cell($w[5], $h,"", 'LB',0,'C'); 
        $this->Cell($w[6], $h,$data->formato, 'LB',0,'C'); 
        $this->Cell($w[7], $h,$data->variedad, 'LB',0,'C'); 
       
        $this->Cell($w[8], $h,$data->numcajas, 'LB',0,'C'); 
        $this->SetFillColor(200,255,220);
       
        $this->Cell($w[9], $h,"SI", 1,0,'C',true); 
        
        
                 
    
        

       
    }
}
