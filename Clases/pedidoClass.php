<?php
class pedido{
    public $cultivo;
    public $formato;
    public $fechapedido;
    public $ggn;
    public $oidpedido;
    public $cliente;
    public $destino;
    public $palletid;
    public $oidsocio;
    public $apellidos;
}

class pedidoCliente{
   
    public $cultivo;
    public $formato;
    public $fechapedido;
    public $cliente;
    public $palletid;
    public $numcajas;
    public $variedad;
    public $ggn;

}
class pedidoClass{

    public function getPedido($oidpedido) {

          
        try{

            $query = "SELECT DISTINCT m.cultivo,m.formato,p.oidpedido, to_char(date(TIMESTAMP 'epoch' + (fechapedido::int) * INTERVAL '1 second' ),'DD/MM/YYYY')as fechapedido,
            p.cliente, p.destino,pc.palletid 
                        ,STRING_AGG(distinct CASE WHEN pt.ggnimpreso<>'' THEN pt.ggnimpreso ELSE pr.ggn END , ',')AS ggn
                        FROM ventas.palletconf pc INNER JOIN ventas.pedido p ON pc.pedidoid=p.oidpedido
                        INNER JOIN materiales m ON pc.oidmaterialconf=m.oidmaterial 
                        INNER JOIN ventas.pallettraza  pt ON pc.palletid= pt.palletid
                        INNER JOIN ventas.recepcionespallets rp ON pt.palletidentrada=rp.idpallet
                        INNER JOIN ventas.recepciones r ON rp.oidalbaran=r.oidalbaran AND rp.albaranlinea=r.albaranlinea
                        INNER JOIN tecnico.productor pr ON r.oidsocio= pr.oidsocio
                        WHERE oidpedido = :oidpedido
                        AND pc.esanulado = 0
                            group by pc.palletid,m.cultivo,m.formato,p.oidpedido,p.cliente, p.destino,pc.palletid 
                        ORDER BY pc.palletid,ggn";

            // Realizamos conexion a db
            $db = conexionClass::getConexion();
            $stmt = $db->prepare($query);
            $stmt->bindParam(":oidpedido", $oidpedido, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_CLASS, "pedido");
            $db = null;
            return $data;
         
   
       }catch(PDOException $e){
           writeLog("Error al recoger los datos del pedido ->". $e->getMessage() );
           echo "Error al recoger los datos del pedido ->". $e->getMessage(); 
           return null;
       }
    }
    public function getAllPedidos($oidcliente,$fecha) {

          
        try{

            $query = "SELECT DISTINCT m.cultivo,m.formato,p.oidpedido, to_char(date(TIMESTAMP 'epoch' + (fechapedido::int) * INTERVAL '1 second' ),'DD/MM/YYYY')as fechapedido,
            p.cliente, p.destino,pc.palletid 
                        ,STRING_AGG(distinct CASE WHEN pt.ggnimpreso<>'' THEN pt.ggnimpreso ELSE pr.ggn END , ',')AS ggn
                        FROM ventas.palletconf pc INNER JOIN ventas.pedido p ON pc.pedidoid=p.oidpedido
                        INNER JOIN materiales m ON pc.oidmaterialconf=m.oidmaterial 
                        INNER JOIN ventas.pallettraza  pt ON pc.palletid= pt.palletid
                        INNER JOIN ventas.recepcionespallets rp ON pt.palletidentrada=rp.idpallet
                        INNER JOIN ventas.recepciones r ON rp.oidalbaran=r.oidalbaran AND rp.albaranlinea=r.albaranlinea
                        INNER JOIN tecnico.productor pr ON r.oidsocio= pr.oidsocio
                        where p.oidcliente = :oidcliente
                        and fechapedido = :fecha
                        AND pc.esanulado = 0
                            group by pc.palletid,m.cultivo,m.formato,p.oidpedido,p.cliente, p.destino,pc.palletid 
                        ORDER BY p.oidpedido,pc.palletid,ggn";

            // Realizamos conexion a db
            $db = conexionClass::getConexion();
            $stmt = $db->prepare($query);
            $stmt->bindParam(":oidcliente", $oidcliente, PDO::PARAM_INT);
            $stmt->bindParam(":fecha", $fecha, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_CLASS, "pedido");
            $db = null;
            return $data;
         
   
       }catch(PDOException $e){
           writeLog("Error al recoger los datos del pedido ->". $e->getMessage() );
           echo "Error al recoger los datos del pedido ->". $e->getMessage(); 
           return null;
       }
    }
    public function getPedidoByCliente($oidcliente,$fecha) {

          
        try{
            $subquery = "SELECT ggn from (SELECT CASE WHEN pt.ggnimpreso<>'' THEN pt.ggnimpreso ELSE pr.ggn END AS ggn,pt.palletid
                        FROM ventas.palletconf pc 
                        INNER JOIN ventas.pedido p ON pc.pedidoid=p.oidpedido
                        INNER JOIN materiales m ON pc.oidmaterialconf=m.oidmaterial 
                        INNER JOIN cultivo cul on cul.oidcultivo=m.oidcultivo
                        INNER JOIN ventas.pallettraza  pt ON pc.palletid= pt.palletid
                        INNER JOIN public.cultivovariedad cv on cv.oidvariedad = pt.oidvariedadtraza
                        left JOIN ventas.recepcionespallets rp ON rp.idpallet = pt.palletidentrada
                        left JOIN ventas.recepciones r ON rp.oidalbaran=r.oidalbaran AND rp.albaranlinea=r.albaranlinea
                        left JOIN tecnico.productor pr ON r.oidsocio= pr.oidsocio
            			where p.oidcliente = :oidcliente
                        and fechapedido = :fecha
                        AND pc.esanulado = 0
						and pt.cajas_palletconfvariedad <> 0
                        ) as tabla where ggn is not null and tabla.palletid = pt.palletid  limit 1 ";

            $query = "SELECT cul.cultivoin as cultivo,m.formato, to_char(date(TIMESTAMP 'epoch' + (fechapedido::int) * INTERVAL '1 second' ),'DD/MM/YYYY')as fechapedido,
            p.cliente,pc.palletid ,sum(pt.cajas_palletconfvariedad) as numcajas,cv.variedad
                        ,CASE WHEN (CASE WHEN pt.ggnimpreso<>'' THEN pt.ggnimpreso ELSE pr.ggn END) IS null 
                        then ({{subquery}})
                        else (CASE WHEN pt.ggnimpreso<>'' THEN pt.ggnimpreso ELSE pr.ggn END) end AS ggn
                        FROM ventas.palletconf pc 
                        INNER JOIN ventas.pedido p ON pc.pedidoid=p.oidpedido
                        INNER JOIN materiales m ON pc.oidmaterialconf=m.oidmaterial 
                        INNER JOIN cultivo cul on cul.oidcultivo=m.oidcultivo
                        INNER JOIN ventas.pallettraza  pt ON pc.palletid= pt.palletid
                        INNER JOIN public.cultivovariedad cv on cv.oidvariedad = pt.oidvariedadtraza
                        left JOIN ventas.recepcionespallets rp ON pt.palletidentrada=rp.idpallet
                        left JOIN ventas.recepciones r ON rp.oidalbaran=r.oidalbaran AND rp.albaranlinea=r.albaranlinea
                        left JOIN tecnico.productor pr ON r.oidsocio= pr.oidsocio
            			where p.oidcliente = :oidcliente
						and fechapedido = :fecha
                        AND pc.esanulado = 0
						and pt.cajas_palletconfvariedad <> 0
                        group by pc.palletid,8,cul.cultivoin,m.formato,p.cliente,fechapedido,m.variedad,cv.variedad,p.oidpedido
                        ORDER BY p.oidpedido,pc.palletid, ggn";

            $query = str_replace("{{subquery}}",$subquery,$query);
            // Realizamos conexion a db
            $db = conexionClass::getConexion();
            $stmt = $db->prepare($query);
            $stmt->bindParam(":oidcliente", $oidcliente, PDO::PARAM_INT);
            $stmt->bindParam(":fecha", $fecha, PDO::PARAM_STR);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_CLASS, "pedidoCliente");
            $db = null;
            return $data;
         
   
       }catch(PDOException $e){
           writeLog("Error al recoger los datos del pedido ->". $e->getMessage() );
           echo "Error al recoger los datos del pedido ->". $e->getMessage(); 
           return null;
       }
    }



    

   
}
?>