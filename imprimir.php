﻿<?php
    include "Funciones/writelog.php";
    include "Clases/conexion.php";
    include "Resources/FPDF/fpdf.php";
    include "Clases/informeClass.php";
    include "Clases/informeClienteClass.php";
    include "Clases/pedidoClass.php";
    include "Funciones/formatFecha.php";

    $url = $_SERVER["REQUEST_URI"];
    writeLog($url);


  
   
    try{
            //parametros de la request
        if(isset($_REQUEST['fechapedido']) && isset( $_REQUEST['oidcliente']) && isset( $_REQUEST['oidpedido']) ){
          $oidpedido = $_REQUEST['oidpedido'];
          $fechaPedido = $_REQUEST['fechapedido'];
          $oidcliente = $_REQUEST['oidcliente'];

          
            //Buscamos oidpedido y oidlinea
            $pedidoClass = new pedidoClass;
            if($oidcliente == 0){
                $pedidos = $pedidoClass->getPedido($oidpedido);

                    
                if($pedidos != null){
                    //print_r($pedidos);
                    $pdf = new PDF('L','mm','A4');
                    $pdf->AliasNbPages();
                    $pdf->SetContentHeader($pedidos[0]->oidpedido);
                    $pdf->AddPage();
                    $oidpallet= -1;
                    $pdf->SetEsPrimero(true);
                    $header = array('Nº PALET', 'GGN1', 'GGN2', 'GGN3', 'GGN4', 'GGN5', 'GGN6', 'GGN7', 'GGN8', 'GGN9', 'GGN10');
                
                    foreach($pedidos as $pedido){
                                
                            $pdf->ImprovedTable($header, $pedido);
                            $pdf->SetEsPrimero(false);

                    }

                    $pdf->Output('D', "Informe_incidencia.pdf", true);
                }else{
                    echo "No se obtuvo información referente al pedido->".$oidpedido;
                    writeLog("Error tras la consulta -> No se obtuvo información referente al pedido->".$oidpedido);
                }
            }else{
                $pedidos = $pedidoClass->getPedidoByCliente($oidcliente, $fechaPedido);
                
                if($pedidos != null){
                    $pdf = new PDFCLIENTE('L','mm','A4');
                    $pdf->AliasNbPages();
                    $pdf->SetContentHeader($pedidos[0]->fechapedido);
                    $pdf->AddPage();
                    $oidpallet= -1;
                    $pdf->SetEsPrimero(true);
                    $header = array('LOADING DAY', 'PICKING DAY', 'PALLET Nº', 'GGN Nº','PRODUCT','LORRY REGISTRATION Nº','COUNT CASE', 'VARIETY', 'BOXES', 'AGRICULTOR');
                    $numrow = 0;

                    foreach($pedidos as $pedido){
                        if($numrow == 21){
                            $pdf->AliasNbPages();
                            $pdf->SetContentHeader($pedidos[0]->fechapedido);
                            $pdf->AddPage();
                            $numrow = 0;
                        }
                        $pdf->ImprovedTable($header, $pedido);
                        $pdf->SetEsPrimero(false);
                        $numrow++;

                    }

                    $pdf->Output('D', "Informe2_incidencia.pdf", true);
                
                }else{
                    echo "No se obtuvo información referente al cliente->".$oidcliente;
                    writeLog("Error tras la consulta -> No se obtuvo información referente al cliente->".$oidcliente);
                }
            }

        }else{
            throw new Exception("No se recogieron todos los parametros");
        }

        
        
    
    }catch(Exception $e){
        echo "Hubo un error en el proceso";
        writeLog("Hubo un error en el proceso-> "+$e->getMessage());
    }
