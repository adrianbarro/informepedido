<?php
header('Access-Control-Allow-Origin: *');
include "Funciones/writelog.php";
include "Clases/conexion.php";
include "Clases/pedidoClass.php";
include "Funciones/formatFecha.php";
include "Clases/excelClass.php";

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\RichText\RichText; 
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Borders;


$url = $_SERVER["REQUEST_URI"];
writeLog($url);

//parametros de la request
$oidpedido = $_REQUEST['oidpedido'];
$fechaPedido = $_REQUEST['fechapedido'];
$oidcliente = $_REQUEST['oidcliente'];
$web = isset($_REQUEST['web'])? true : false;


try {
     if(isset($_REQUEST['fechapedido']) && isset( $_REQUEST['oidcliente']) && isset( $_REQUEST['oidpedido']) ){
          $oidpedido = $_REQUEST['oidpedido'];
          $fechaPedido = $_REQUEST['fechapedido'];
          $oidcliente = $_REQUEST['oidcliente'];
     
    //Buscamos oidpedido y oidlinea
    $pedidoClass = new pedidoClass;
   

    if($oidpedido == 0){
        $pedidos = $pedidoClass->getPedidoByCliente($oidcliente, $fechaPedido);
       
        $oidpallet =0;
        if ($pedidos != null) {
        
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->getColumnDimension('A')->setWidth(18);
            $sheet->getColumnDimension('B')->setWidth(18);
            $sheet->getColumnDimension('C')->setWidth(15);
            $sheet->getColumnDimension('D')->setWidth(20);
            $sheet->getColumnDimension('E')->setWidth(18);
            $sheet->getColumnDimension('F')->setWidth(24);
            $sheet->getColumnDimension('G')->setWidth(23);
            $sheet->getColumnDimension('H')->setWidth(20);
            $sheet->getColumnDimension('I')->setWidth(13);
            $sheet->getColumnDimension('J')->setWidth(15);
          
            $celda = 2;

            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Paid');
            $drawing->setDescription('Paid');
            $drawing->setPath('Img/LOGO ONUBAFRUIT.png'); // put your path and image here
            $drawing->setCoordinates('E2');
            $drawing->setWidth('550'); 
            $drawing->setWorksheet($spreadsheet->getActiveSheet());

            $richText = new RichText();
            $payable = $richText->createTextRun('Packing List');
            $payable->getFont()->setBold(true);
            $sheet->getCell('A' . $celda)->setValue($richText);
            $celda++;
            $sheet->setCellValue('A' . $celda, $pedidos[0]->fechapedido."    V12");
            $celda = $celda+4;
            $richText = new RichText();
            $payable = $richText->createTextRun('COOPERATIVE');
            $payable->getFont()->setBold(true);
            $sheet->getCell('A' . $celda)->setValue($richText);
            $sheet->getStyle('A' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
            $sheet->setCellValue('B' . $celda, "SAT CONDADO" );
            $celda ++;
            $richText = new RichText();
            $payable = $richText->createTextRun('CUSTOMER');
            $payable->getFont()->setBold(true);
            $sheet->getCell('A' . $celda)->setValue($richText);
            $sheet->getStyle('A' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
            $sheet->setCellValue('B' . $celda, $pedidos[0]->cliente);
            $celda++;
            // $richText = new RichText();
            // $payable = $richText->createTextRun('DESTINO');
            // $payable->getFont()->setBold(true);
            // $sheet->getCell('A' . $celda)->setValue($richText);
            // $sheet->setCellValue('B' . $celda, $pedidos[0]->destino);
            // $celda++;
            // $richText = new RichText();
            // $payable = $richText->createTextRun('CULTIVO');
            // $payable->getFont()->setBold(true);
            // $sheet->getCell('A' . $celda)->setValue($richText);
            // $sheet->setCellValue('B' . $celda, $pedidos[0]->cultivo);
            // $celda++;
            // $richText = new RichText();
            // $payable = $richText->createTextRun('FORMATO');
            // $payable->getFont()->setBold(true);
            // $sheet->getCell('A' . $celda)->setValue($richText);
            // $sheet->setCellValue('B' . $celda, $pedidos[0]->formato);
            // $celda++;
            // $richText = new RichText();
            // $payable = $richText->createTextRun('FECHA');
            // $payable->getFont()->setBold(true);
            // $sheet->getCell('A' . $celda)->setValue($richText);
            // $sheet->setCellValue('B' . $celda, $pedidos[0]->fechapedido);
            $celda++;
           
                    $richText = new RichText();
                    $payable = $richText->createTextRun('LOADING DAY');
                    $payable->getFont()->setBold(true);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $sheet->getStyle('A' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
                    $richText = new RichText();
                    $payable = $richText->createTextRun('PICKING DAY');
                    $payable->getFont()->setBold(true);
                    $sheet->getCell('B' . $celda)->setValue($richText);
                    $sheet->getStyle('B' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
                    $richText = new RichText();
                    $payable = $richText->createTextRun('PALLET Nº');
                    $payable->getFont()->setBold(true);
                    $sheet->getCell('C' . $celda)->setValue($richText);
                    $sheet->getStyle('C' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
                    $richText = new RichText();
                    $payable = $richText->createTextRun('GGN Nº');
                    $payable->getFont()->setBold(true);
                    $sheet->getCell('D' . $celda)->setValue($richText);
                    $sheet->getStyle('D' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
                    $richText = new RichText();
                    $payable = $richText->createTextRun('PRODUCT');
                    $payable->getFont()->setBold(true);
                    $sheet->getCell('E' . $celda)->setValue($richText);
                    $sheet->getStyle('E' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
                    $richText = new RichText();
                    $payable = $richText->createTextRun('LORRY REGISTRATION Nº');
                    $payable->getFont()->setBold(true);
                    $sheet->getCell('F' . $celda)->setValue($richText);
                    $sheet->getStyle('F' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
                    $richText = new RichText();
                    $payable = $richText->createTextRun('COUNT CASE');
                    $payable->getFont()->setBold(true);
                    $sheet->getCell('G' . $celda)->setValue($richText);
                    $sheet->getStyle('G' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
                    $richText = new RichText();
                    $payable = $richText->createTextRun('VARIETY');
                    $payable->getFont()->setBold(true);
                    $sheet->getCell('H' . $celda)->setValue($richText);
                    $sheet->getStyle('H' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
                    $richText = new RichText();
                    $payable = $richText->createTextRun('BOXES');
                    $payable->getFont()->setBold(true);
                    $sheet->getCell('I' . $celda)->setValue($richText);
                    $sheet->getStyle('I' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
                    $richText = new RichText();
                    $payable = $richText->createTextRun('AGRICULTOR');
                    $payable->getFont()->setBold(true);
                    $sheet->getCell('J' . $celda)->setValue($richText);
                    $sheet->getStyle('J' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87BDDC');
                  
            
            // Redireccionamos para que descargue el archivo generado
            
            $celda++;

            foreach ($pedidos as $pedido) {
             
                $position = 0;//determina la posicion de la celda
                

                $sheet->setCellValueExplicit(
                   'A'.$celda,
                   $pedido->fechapedido,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit(
                   'B'.$celda,
                   $pedido->fechapedido,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit(
                   'C'.$celda,
                   $pedido->palletid,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit(
                   'D'.$celda,
                   $pedido->ggn,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit(
                   'E'.$celda,
                   $pedido->cultivo,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit(
                   'F'.$celda,
                    "",
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit(
                   'G'.$celda,
                   $pedido->formato,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit(
                   'H'.$celda,
                   $pedido->variedad,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit(
                   'I'.$celda,
                   $pedido->numcajas,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit(
                    'J'.$celda,
                    "SI",
                 \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                 $sheet->getStyle('J' . $celda)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('87DC9B');

                 $celda++;
              
            
            }
            $writer = new Xlsx($spreadsheet);
            $writer->save('Temp/informePedido.xlsx');
            header("Location: Temp/informePedido.xlsx");
        
        } else {
            echo "No se obtuvo información referente al pedido->" . $oidpedido;
            writeLog("Error tras la consulta -> No se obtuvo información referente al pedido->" . $oidpedido);
        }
    }else{

        $pedidos = $pedidoClass->getAllPedidos($oidcliente,$fechaPedido);
        $hojasExcel = array();
        $cliente = $pedidos[0]->cliente;
        $fecha = $pedidos[0]->fechapedido;
        
        if ($pedidos != null) {
            $pedidoid = null;
            foreach($pedidos as $pedido){
                
                if($pedidoid != $pedido->oidpedido){
                    //echo "nuevo pedido ".$pedido->oidpedido."<br>";

                    
                    $pedidoid = $pedido->oidpedido;
                    
                    $spreadsheet = new Spreadsheet();
                    $sheet = $spreadsheet->getActiveSheet();
                    $sheet->getColumnDimension('A')->setWidth(12);
                    $sheet->getColumnDimension('B')->setWidth(20);
                    $sheet->getColumnDimension('C')->setWidth(20);
                    $sheet->getColumnDimension('D')->setWidth(20);
                    $sheet->getColumnDimension('E')->setWidth(20);
                    $sheet->getColumnDimension('F')->setWidth(20);
                    $sheet->getColumnDimension('G')->setWidth(20);
                    $sheet->getColumnDimension('H')->setWidth(20);
                    $sheet->getColumnDimension('I')->setWidth(20);
                    $sheet->getColumnDimension('J')->setWidth(20);
                    $sheet->getColumnDimension('K')->setWidth(20);
                    $celda = 1;

                    $styleArray = array(
                        'borders' => array(
                            'outline' => array(
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => array('argb' => 'FFFFFFFF'),
                            ),
                        ),
                    );
                    
                    

                    $richText = new RichText();
                    $payable = $richText->createTextRun('PEDIDO');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $sheet->setCellValue('C' . $celda,$pedido->oidpedido );
                    $celda = $celda + 2;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('CLIENTE');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $sheet->setCellValue('C' . $celda, $pedido->cliente);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('NOMBRE DEL PROVEEDOR');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $sheet->setCellValue('C' . $celda, "SAT CONDADO HUELVA");
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('DESTINO');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $sheet->setCellValue('C' . $celda, $pedido->destino);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('IAN Lidl');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('PRODUCTO');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $sheet->setCellValue('C' . $celda, $pedido->cultivo);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('VARIEDAD');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('CALIBRE');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('COLOR DE CARNE(fruta de hueso)');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('LOTE 1');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('LOTE 2');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('FORMATO');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $sheet->setCellValue('C' . $celda, $pedido->formato);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('FECHA');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $sheet->setCellValue('C' . $celda, $pedido->fechapedido);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('MATRICULA DEL CAMIÓN');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $celda++;
                    $richText = new RichText();
                    $payable = $richText->createTextRun('En caso de un solo GGN en un camion entero anotelo aquí');
                    $payable->getFont()->setBold(true);
                    $sheet->mergeCells("A".$celda.":B".$celda);
                    $sheet->getCell('A' . $celda)->setValue($richText);
                    $sheet->mergeCells("C".$celda.":G".$celda);
                    $celda++;
                    $celda = $celda + 2;
                            $richText = new RichText();
                            $payable = $richText->createTextRun('Nº PALLET');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('A' . $celda)->setValue($richText);
                            $richText = new RichText();
                            $payable = $richText->createTextRun('GGN1');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('B' . $celda)->setValue($richText);
                            $richText = new RichText();
                            $payable = $richText->createTextRun('GGN2');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('C' . $celda)->setValue($richText);
                            $richText = new RichText();
                            $payable = $richText->createTextRun('GGN3');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('D' . $celda)->setValue($richText);
                            $richText = new RichText();
                            $payable = $richText->createTextRun('GGN4');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('E' . $celda)->setValue($richText);
                            $richText = new RichText();
                            $payable = $richText->createTextRun('GGN5');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('F' . $celda)->setValue($richText);
                            $richText = new RichText();
                            $payable = $richText->createTextRun('GGN6');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('G' . $celda)->setValue($richText);
                            $richText = new RichText();
                            $payable = $richText->createTextRun('GGN7');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('H' . $celda)->setValue($richText);
                            $richText = new RichText();
                            $payable = $richText->createTextRun('GGN8');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('I' . $celda)->setValue($richText);
                            $richText = new RichText();
                            $payable = $richText->createTextRun('GGN9');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('J' . $celda)->setValue($richText);
                            $richText = new RichText();
                            $payable = $richText->createTextRun('GGN10');
                            $payable->getFont()->setBold(true);
                            $sheet->getCell('K' . $celda)->setValue($richText);
                    
                    
                    // Redireccionamos para que descargue el archivo generado
                    
                    $celda++;

                }
                    $ggn = explode(",",$pedido->ggn);
                    $proceso = true;
                    $celdas = array('A'.$celda,'B'.$celda,"C".$celda,"D".$celda,"E".$celda,"F".$celda,"G".$celda,"H".$celda,"I".$celda,"J".$celda,"K".$celda);
                    $position = 0;//determina la posicion de la celda
                    $i = 0;

                    while($proceso){
                        $incremento = true;
                        if($i < count($ggn)){
                            if($position != 10){
                                if($position == 0){
                                    $sheet->setCellValue($celdas[$position], $i == 0 ? $pedido->palletid : '');
                                }else{
                                    $sheet->setCellValueExplicit(
                                        $celdas[$position],
                                        $ggn[$i],
                                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                                }
                            }else{
                                $sheet->setCellValueExplicit(
                                    $celdas[$position],
                                    $ggn[$i],
                                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                                $celda ++;
                                $celdas = array('A'.$celda,'B'.$celda,"C".$celda,"D".$celda,"E".$celda,"F".$celda,"G".$celda,"H".$celda,"I".$celda,"J".$celda,"K".$celda);
                                $position = 0;
                                $incremento = false;
        
                                if( $i == ((count($ggn))-1) ){
                                    $proceso = false;
                                }
                                
                            }
                        }else{
                            if($position != 10){
                                
                                $sheet->setCellValueExplicit(
                                    $celdas[$position],
                                "",
                                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                            }else{
                                $sheet->setCellValueExplicit(
                                    $celdas[$position],
                                    "",
                                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                                $proceso = false;
                                
                            }
                        }
                        if($position != 0){
                            $i++;
                        }
                        if($incremento){
                            $position++;
                        }
                    }   
                
                    
                    $celda ++;
                    $hojasExcel[$pedido->oidpedido] = $spreadsheet;
                    
                
                
            
            }
           
          //Creo archivo zip temporal
           $zip = new ZipArchive();
           $name= "pedidos_".$cliente."_".$fecha.".zip";
           $zip->open("Temp/miarchivo.zip",ZipArchive::CREATE);
            //Por cada objeto spreedsheet que creamos anteriormente , generamos un .xlsx  en la carpeta temp y lo añadimos al .zip
            foreach(array_keys($hojasExcel) as $key){
               
                $ruta = 'Temp/'.$key.'.xlsx';
                $writer = new Xlsx($hojasExcel[$key]);
                $writer->save($ruta);
                $zip->addFile($ruta,$key.'.xlsx');
        
            }
            //cerramos el.zip y descargamos
            $zip->close();  
            header("Content-type: application/octet-stream");
            header("Content-disposition: attachment; filename=".$name);   
            readfile('Temp/miarchivo.zip'); 

            //Eliminamos archvios temporales          
             
            foreach(array_keys($hojasExcel) as $key){
               
                $ruta = 'Temp/'.$key.'.xlsx';
                unlink($ruta);
    
            }
			
            unlink('Temp/miarchivo.zip');
            
        
        } else {
            echo "No se obtuvo información referente al pedido->" . $oidpedido;
            writeLog("Error tras la consulta -> No se obtuvo información referente al pedido->" . $oidpedido);
        }
    }
  }else{
      
            throw new Exception("No se recogieron todos los parametros");
        
    }
} catch (Exception $e) {
    echo "Hubo un error en el proceso" .$e->getMessage();
    writeLog("Hubo un error en el proceso-> " . $e->getMessage());
}


