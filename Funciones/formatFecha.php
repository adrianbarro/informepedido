<?php

function formatFecha($fecha){
    list($dia,$mes, $año) = explode('/', $fecha);

    return $año."-".$mes."-".$dia;

}
?>