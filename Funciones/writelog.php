<?php
function writeLog($texto)
{
    $nombre = date("Y-m-d") . ".txt";
    $file = fopen("log/".$nombre, "a");
    fwrite($file, "[".date("Y-m-d H:i:s")."] ".$texto." \n");

    fclose($file);
}
?>